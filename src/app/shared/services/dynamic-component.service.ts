import { Injectable } from '@angular/core';
import { AlertsComponent }   from '../../UIComponents/alerts/alerts.component';
import { ButtonsComponent }   from '../../UIComponents/buttons/buttons.component';
import { DynamicComponentItem }   from '../classes/dynamic-component-item';
import { ColorGuideComponent }   from '../../design-elements/color-guide/color-guide.component';
import { FontGuideComponent }   from '../../design-elements/font-guide/font-guide.component';
import { IconGuideComponent } from '../../design-elements/icon-guide/icon-guide.component';
import { LogoGuideComponent } from '../../design-elements/logo-guide/logo-guide.component';
import { MultipleViewComponent } from '../../UIComponents/multiple-view/multiple-view.component';
import { PageTitlesComponent } from '../../UIComponents/page-titles/page-titles.component';
import { InputsComponent } from '../../UIComponents/inputs/inputs.component';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class DynamicComponentService{
  getComponents(type: string) {
    switch(type){
      case 'design':
        return [ new DynamicComponentItem(ColorGuideComponent, {name: 'Color guide Component',link:'color-guide',text:'Color Guide'}),
        new DynamicComponentItem(FontGuideComponent, {name: 'Font guide Component',link:'font-guide',text:'Font Guide'}),
        new DynamicComponentItem(IconGuideComponent, {name: 'Icon guide Component',link:'icon-guide',text:'Icon Guide'}),
        new DynamicComponentItem(LogoGuideComponent, {name: 'Logo guide Component',link:'logo-guide',text:'Logo Guide'})]
        break;
      case 'ui':
        return [
          new DynamicComponentItem(AlertsComponent, {name: 'AlertComponents',link:'alerts',text:'Alert Components', category: "component"}),
          new DynamicComponentItem(ButtonsComponent, {name: 'ButtonComponents',link:'buttons',text:'Button Components', category: "component"}),
          new DynamicComponentItem(MultipleViewComponent, {name: 'MultipleViewComponents', link:'multi-view', text:'Table Components', category: "component"}),
          new DynamicComponentItem(PageTitlesComponent, {name: 'PageTitleComponents',link:'titles',text:'Title Components', category: "component"}),
          new DynamicComponentItem(InputsComponent, {name: 'InputsComponents',link:'inputs',text:'Input Components', category: "element"})
        ];
      break;
    }
    
  }
  getLazyComponents(type: string) {
    switch(type){
      case 'design':
        return [{component: "ColorGuideComponent", name: 'Color guide Component',link:'color-guide',text:'Color Guide',category: "component"}];
        break;
      case 'ui':
        return [
          {component: "AlertsComponent",name: 'AlertComponents',link:'alerts',text:'Alert Components', category: "component"},
          {component: "ButtonsComponent", name: 'ButtonComponents',link:'buttons',text:'Button Components', category: "component"},
          {component: "MultipleViewComponent", name: 'MultipleViewComponents', link:'multi-view', text:'Table Components', category: "component"},
          {component: "PageTitlesComponent", name: 'PageTitleComponents',link:'titles',text:'Title Components', category: "component"},
          {component: "InputsComponent", name: 'InputsComponents',link:'inputs',text:'Input Components', category: "element"}
        ];
        break;
    }
  }
}