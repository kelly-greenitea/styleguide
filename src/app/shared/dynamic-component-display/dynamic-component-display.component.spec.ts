import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicComponentDisplayComponent } from './dynamic-component-display.component';

describe('DynamicComponentDisplayComponent', () => {
  let component: DynamicComponentDisplayComponent;
  let fixture: ComponentFixture<DynamicComponentDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicComponentDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicComponentDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
