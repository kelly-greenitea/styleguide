import { Component, OnInit, Input, ViewChild, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { DynamicComponentItem } from '../classes/dynamic-component-item';
import { ComponentAnchorDirective } from '../directives/component-anchor.directive';
import { DynamicComponent } from '../models/dynamic-component';
@Component({
  selector: 'app-dynamic-component-display',
  templateUrl: './dynamic-component-display.component.html',
  styleUrls: ['./dynamic-component-display.component.css']
})
export class DynamicComponentDisplayComponent implements OnInit {
  @Input() component? : DynamicComponentItem;
  @ViewChild(ComponentAnchorDirective, {static: true}) componentHost: ComponentAnchorDirective;
 
  constructor(private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    this.loadComponent();
  }
  private loadComponent(){
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.component.component);
      this.viewContainerRef = this.componentHost.viewContainerRef;
      this.viewContainerRef.clear();
      const componentRef = this.viewContainerRef.createComponent(componentFactory);
      (<DynamicComponent>componentRef.instance).data = this.component.data;
  }
}
