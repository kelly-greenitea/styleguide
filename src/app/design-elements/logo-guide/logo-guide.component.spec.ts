import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoGuideComponent } from './logo-guide.component';

describe('LogoGuideComponent', () => {
  let component: LogoGuideComponent;
  let fixture: ComponentFixture<LogoGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
