import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignElementsComponent } from './design-elements.component';

describe('DesignElementsComponent', () => {
  let component: DesignElementsComponent;
  let fixture: ComponentFixture<DesignElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
