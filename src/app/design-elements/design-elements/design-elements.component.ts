import { Component, OnInit } from '@angular/core';
import { DynamicComponentService } from '../../shared/services/dynamic-component.service';
import { DynamicComponentItem } from '../../shared/classes/dynamic-component-item';

@Component({
  selector: 'app-design-elements',
  templateUrl: './design-elements.component.html',
  styleUrls: ['./design-elements.component.css']
})
export class DesignElementsComponent implements OnInit {
  public components : DynamicComponentItem[];
  public navList = [{text:'Color Guide',link:'color-guide'}];
  constructor(private componentService: DynamicComponentService) { }

  ngOnInit(): void {
    this.components = this.componentService.getComponents('design');
  }

}
