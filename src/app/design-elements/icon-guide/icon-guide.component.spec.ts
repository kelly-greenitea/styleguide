import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconGuideComponent } from './icon-guide.component';

describe('IconGuideComponent', () => {
  let component: IconGuideComponent;
  let fixture: ComponentFixture<IconGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
