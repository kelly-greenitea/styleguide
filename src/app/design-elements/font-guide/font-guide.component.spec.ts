import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FontGuideComponent } from './font-guide.component';

describe('FontGuideComponent', () => {
  let component: FontGuideComponent;
  let fixture: ComponentFixture<FontGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FontGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FontGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
