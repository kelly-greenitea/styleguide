import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconGuideComponent } from './icon-guide/icon-guide.component';
import { LogoGuideComponent } from './logo-guide/logo-guide.component';
import { FontGuideComponent } from './font-guide/font-guide.component';
import { ColorGuideComponent } from './color-guide/color-guide.component';
import { DesignElementsComponent } from './design-elements/design-elements.component';



@NgModule({
  declarations: [IconGuideComponent,LogoGuideComponent,FontGuideComponent,ColorGuideComponent,DesignElementsComponent],
  imports: [
    CommonModule
  ]
})
export class DesignElementsModule { }
