import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UicomponentsComponent } from './UIComponents/uicomponents/uicomponents.component';
import { ColorGuideComponent } from './design-elements/color-guide/color-guide.component';
import { FontGuideComponent } from './design-elements/font-guide/font-guide.component';
import { LogoGuideComponent } from './design-elements/logo-guide/logo-guide.component';
import { AlertsComponent } from './UIComponents/alerts/alerts.component';
import { ButtonsComponent } from './UIComponents/buttons/buttons.component';
import { DesignElementsComponent } from './design-elements/design-elements/design-elements.component';
import { PreloaderComponent } from './UIComponents/preloader/preloader.component';
import { PageTitlesComponent } from './UIComponents/page-titles/page-titles.component';
import { SingleViewComponent } from './UIComponents/single-view/single-view.component';
import { MultipleViewComponent } from './UIComponents/multiple-view/multiple-view.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { ComponentAnchorDirective } from './shared/directives/component-anchor.directive';
import { DynamicComponentDisplayComponent } from './shared/dynamic-component-display/dynamic-component-display.component';
import { SideNavigationComponent } from './shared/side-navigation/side-navigation.component';
import { IconGuideComponent } from './design-elements/icon-guide/icon-guide.component';

@NgModule({
  declarations: [
    AppComponent,
    UicomponentsComponent,
    ColorGuideComponent,
    FontGuideComponent,
    LogoGuideComponent,
    AlertsComponent,
    ButtonsComponent,
    DesignElementsComponent,
    PreloaderComponent,
    PageTitlesComponent,
    SingleViewComponent,
    MultipleViewComponent,
    NavigationComponent,
    ComponentAnchorDirective,
    DynamicComponentDisplayComponent,
    SideNavigationComponent,
    IconGuideComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
