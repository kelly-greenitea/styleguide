import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UicomponentComponent } from './uicomponent.component';

describe('UicomponentComponent', () => {
  let component: UicomponentComponent;
  let fixture: ComponentFixture<UicomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UicomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UicomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
