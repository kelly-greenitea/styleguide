import { Component, OnInit, ViewChild } from '@angular/core';
import { DynamicComponentService } from '../../shared/services/dynamic-component.service';
import { DynamicComponentItem } from '../../shared/classes/dynamic-component-item';


@Component({
  selector: 'app-uicomponents',
  templateUrl: './uicomponents.component.html',
  styleUrls: ['./uicomponents.component.css']
})

export class UicomponentsComponent implements OnInit  {
  public components : DynamicComponentItem[];
  public lazyComponents: any[];
  public navList = []; // = [{text:'Alert Components',link:'ui-component#alert-components'},
  //{text:'Button Components',link:'#button-components'}];
  constructor(private componentService: DynamicComponentService) { }

  ngOnInit() {
   this.getComponents();
  }

  private getComponents(){
    this.components = this.componentService.getComponents('ui');
    console.log(this);
    let categories = [{title: "Components", category: "component"},{title: "Elements", category: "element"}];
    this.components.forEach((c,i) =>  {
      let cti = categories.findIndex(cx => cx.category == c.data['category']);
      if(cti > -1){
        let txt = categories[cti]['title'];
        categories.splice(cti,1);
        this.navList.push({text : txt, link : "", title : true});
      }
      this.navList.push({text : c.data['text'], link : "ui-component#"+c.data['link'], title : false});
    });
    // categories.forEach((ct,i) => {
    //   if(i == 0 || i > 0 && ct.category != )
    //   this.navList.splice(i,0,{text : ct.title, link : "", title : true});
    // });
    //this.navList.splice(0,0,);

  }  
}