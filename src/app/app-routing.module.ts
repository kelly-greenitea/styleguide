import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UicomponentsComponent } from './UIComponents/uicomponents/uicomponents.component';
import { DesignElementsComponent } from './design-elements/design-elements/design-elements.component';

const routes: Routes = [
  {path:'ui-component', component: UicomponentsComponent},
  {path:'design-elements', component: DesignElementsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
